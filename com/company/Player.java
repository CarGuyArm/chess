package com.company;
import java.util.Scanner;

public class Player {
    private String name1,name2;
    public byte NumOfPlayers(){
        Scanner input = new Scanner(System.in);
        System.out.print("Input Number Of Players: ");
        int number = input.nextByte();

        if(number==1){
            name1=input.next();
            System.out.println("You will play with system.");
            return 1;
        }
        else if(number==2){
            name1=input.next();
            name2=input.next();
            System.out.println("Player 1-"+name1+" will play against Player 2-"+name2+".");
            return 2;
        }
        else return 0;

    }
}
